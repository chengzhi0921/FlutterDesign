import 'package:json_annotation/json_annotation.dart';
/*
 * 快递<br/>
 */
part 'Parcel.g.dart';
@JsonSerializable()
class Parcel  {

  String time;

  String ftime;

  String context;

  String location;

  Parcel();

  factory Parcel.fromJson(Map<String, dynamic> json) => _$ParcelFromJson(json);

  Map<String, dynamic> toJson() => _$ParcelToJson(this);

}
