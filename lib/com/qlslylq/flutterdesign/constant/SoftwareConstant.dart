/*
 * 应用常量<br/>
 */
class SoftwareConstant {

/*
 * 分页数量<br/>
 */
  static final int PAGER_SIZE = 15;

/*
 * 最大分页数量<br/>
 */
  static final int MAX_PAGER_SIZE = 999;

/*
 * 空数据标记位<br/>
 */
  static final String NULL = "null";

/*
 * 分页初始值<br/>
 */
  static final int PAGER_INIT_INDEX = 1;

/*
 * sdcard路径标记<br/>
 * 用于区分网络路径与设备路径<br/>
 */
  static final String SDCARD_FLAG = "storage";

/*
 * 默认地图缩放级别<br/>
 */
  static final int MAP_ZOOM = 12;

/*
 * 默认地图俯仰角0°~45°（垂直与地图时为0<br/>
 */
  static final double MAP_TILT = 38.5;

/*
 * 默认地图偏航角 0~360° (正北方为0)<br/>
 */
  static final double MAP_BEARING = 300;

/*
 * 默认地图插入区域与view的边框之间的空白距离<br/>
 * 解决Marker在屏幕外问题<br/>
 */
  static final int MAP_PADDING = 100;
}
