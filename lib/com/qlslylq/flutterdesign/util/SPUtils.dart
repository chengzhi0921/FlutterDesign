import 'package:shared_preferences/shared_preferences.dart';

/*
 * 本地存储工具类<br/> 
 */
class SPUtils {

  /*
   * 保存在手机里面的文件名
   */
  static final String FILE_NAME = "flutter_design";

  /*
   * 用户帐号信息
   */
  static final String MEMBER_INFO = "member_info";
  static final String MEMBER_TOKEN = "member_token";

  /*
   * APP唯一标示
   */
  static final String APP_UUID = "app_uuid";

  /*
   * 是否第一次启动
   */
  static final String IS_FIRST_START = "is_first_start";


  /*
   * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
   */
  static void put(String key, Object object) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    if (object is String) {
      sp.setString(key, object);
    } else if (object is int) {
      sp.setInt(key, object);
    } else if (object is bool) {
      sp.setBool(key, object);
    } else if (object is double) {
      sp.setDouble(key, object);
    } else if (object is List<String>) {
      sp.setStringList(key, object);
    }
  }

  /*
   * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
   */
  static Object get(String key, Object defaultObject) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    if (defaultObject is String) {
      sp.getString(key);
    } else if (defaultObject is int) {
      sp.getInt(key);
    } else if (defaultObject is bool) {
      sp.getBool(key);
    } else if (defaultObject is double) {
      sp.getDouble(key);
    } else if (defaultObject is List<String>) {
      sp.getStringList(key);
    }
  }

  /*
   * 清除所有数据
   */
  static void clear() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.clear();
  }

}
